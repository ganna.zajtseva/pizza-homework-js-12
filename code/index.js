import { pizzaBD, pizzaUser } from "./data-pizza.js";
import { show, runningBunner, validate, selectInput } from "./functions.js";

const priceElem = document.getElementById("price");
const sauceElem = document.getElementById("sauce");
const toppingElem = document.getElementById("topping");
const pizzaCake = document.querySelector(".table");
const form = document.querySelector("#pizza");
const catchSale = document.querySelector("#banner");

form.addEventListener("click", (e) => {
  if (e.target.tagName === "INPUT" && e.target.checked) {
    pizzaUser.size = pizzaBD.size.find((el) => el.name === e.target.id);

    show(pizzaBD, pizzaUser, priceElem, sauceElem, toppingElem);
  }
});

let dragged = null;

document.querySelector(".ingridients").addEventListener("dragstart", (e) => {
  dragged = e.target;
});

pizzaCake.addEventListener("dragover", (e) => {
  e.preventDefault();
});

pizzaCake.addEventListener("drop", (e) => {
  e.preventDefault();
  switch (dragged.id) {
    case "sauceClassic":
    case "sauceBBQ":
    case "sauceRikotta":
      if (pizzaUser.sauce) {
        const sauce = document.querySelector(`img#img_${pizzaUser.sauce.name}`);
        if (sauce) {
          sauce.remove();
        }
      }
      pizzaUser.sauce = pizzaBD.sauce.find((el) => el.name === dragged.id);
      break;
    case "moc1":
    case "moc2":
    case "moc3":
    case "telya":
    case "vetch1":
    case "vetch2":
      pizzaUser.topping.push(
        pizzaBD.topping.find((el) => el.name === dragged.id)
      );
      break;
  }
  show(pizzaBD, pizzaUser, priceElem, sauceElem, toppingElem);
  if (dragged.tagName === "IMG")
    pizzaCake.insertAdjacentHTML(
      "beforeend",
      `<img src = "${dragged.src}" id= "img_${dragged.id}">`
    );
});

show(pizzaBD, pizzaUser, priceElem, sauceElem, toppingElem);

catchSale.addEventListener("mouseover", (e) => {
  runningBunner(catchSale);
});

const inputs = document.querySelectorAll(".grid input");
[...inputs].forEach((input) => {
  if (input.type === "text" || input.type === "tel" || input.type === "email") {
    console.log("===");
    input.addEventListener("change", () => {
      if (input.type === "text" && validate(/^[А-я-ії\'єґ]{2,}$/i, input.value))
        selectInput(input, pizzaUser, "userName");
      else if (input.type === "tel" && validate(/^\+380\d{9}$/, input.value))
        selectInput(input, pizzaUser, "userPhone");
      else if (
        input.type === "email" &&
        validate(/^[a-z0-9_.]{3,}@[a-z0-9.]{2,}\.[a-z.]{2,9}$/, input.value)
      )
        selectInput(input, pizzaUser, "userEmail");
      else {
        input.classList.add("error");
      }
    });
  } else if (input.type === "reset") {
    input.addEventListener("click", () => {});
  }
});
const submit = document.getElementById("btnSubmit");
submit.addEventListener("click", () => {
  const json = JSON.stringify(pizzaUser);
  localStorage.userInfo = json;
});
