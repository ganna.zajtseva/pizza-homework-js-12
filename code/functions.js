export function removeTopping(
  topping,
  pizBD,
  pizzaUsr,
  priceElement,
  sauceElement,
  toppingElement
) {
  const idx = pizzaUsr.topping.findIndex((el) => el.name == topping);
  if (idx >= 0) {
    pizzaUsr.topping.splice(idx, 1);
  }

  show(pizBD, pizzaUsr, priceElement, sauceElement, toppingElement);
}

export function show(
  pizBD,
  pizzaUsr,
  priceElement,
  sauceElement,
  toppingElement
) {
  let totalPrice = 0;
  if (pizzaUsr.size) {
    totalPrice += parseFloat(pizzaUsr.size.price);
  }
  if (pizzaUsr.sauce) {
    totalPrice += parseFloat(pizzaUsr.sauce.price);
  }
  if (pizzaUsr.topping.length) {
    totalPrice += pizzaUsr.topping.reduce(
      (total, toppingElement) => total + toppingElement.price,
      0
    );
  }
  priceElement.innerText = totalPrice;
  pizzaUsr.price = totalPrice;
  pizzaUsr.data = new Date();
  if (pizzaUsr.sauce) {
    sauceElement.innerHTML = `<br/><span class="topping">${pizzaUsr.sauce.productName}</span>`;
  }
  if (Array.isArray(pizzaUsr.topping)) {
    const groupedtoppingElements = pizzaUsr.topping.reduce((total, el) => {
      total[el.name] = (total[el.name] || 0) + 1;

      return total;
    }, {});

    toppingElement.innerHTML = "";
    for (const propName in groupedtoppingElements) {
      const tpng = pizBD.topping.find((el) => el.name == propName);
      if (tpng) {
        toppingElement.innerHTML += `<span class="topping">${tpng.productName}</span><span class="count">${groupedtoppingElements[propName]}</span>`;

        const deltoppingElementSpan = document.createElement("span");
        deltoppingElementSpan.id = `del_${tpng.name}`;
        deltoppingElementSpan.innerHTML = "&#x274C;";
        deltoppingElementSpan.classList = "del";

        toppingElement.appendChild(deltoppingElementSpan);
        toppingElement.innerHTML += "<br/>";
      }
    }

    for (const propName in groupedtoppingElements) {
      const tpng = pizBD.topping.find((el) => el.name == propName);
      if (tpng) {
        const deltoppingElementSpan = toppingElement.querySelector(
          `#del_${tpng.name}`
        );
        deltoppingElementSpan.addEventListener("click", (e) => {
          removeTopping(
            e.target.id.substring(4),
            pizBD,
            pizzaUsr,
            priceElement,
            sauceElement,
            toppingElement
          );
          document
            .querySelector(`img#img_${e.target.id.substring(4)}`)
            .remove();
        });
      }
    }
  }
}

export function runningBunner(catchSale) {
  catchSale.style.bottom = `${Math.floor(Math.random() * 70)}%`;
  catchSale.style.right = `${Math.floor(Math.random() * 70)}%`;
}

export function validate(pattern, value) {
  return pattern.test(value);
}

export function selectInput(input, data, prop) {
  input.className = "";
  input.classList.add("success");
  data[prop] = input.value;
}

